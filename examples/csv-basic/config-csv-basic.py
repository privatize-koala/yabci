import yabci


CONFIG = [
    yabci.Importer(

        name="CSV Sample Importer Basic",

        target_account="Assets:FooBank:Account1",

        # where are transactions stored? benedict lib puts csv values into a
        # sub-dict "values". "values" contains a list, each entry is mapped
        # into a single transaction
        mapping_transactions="values",

        # how shall a sinle transaction be mapped?
        mapping_transaction={
            "date": "Date",
            "narration": "Description",
            "postings": [
                {
                    "amount": ["Amount", b"EUR"],
                }
            ],
        },
    ),
]
