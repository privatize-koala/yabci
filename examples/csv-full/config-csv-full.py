import sys
import re
import yabci
from smart_importer import apply_hooks, PredictPostings
from beancount.ingest.scripts_utils import ingest

CONFIG = [
    yabci.Importer(

        name="CSV Sample Importer",

        # optionally only consider files with certain names. pattern is a regex
        file_pattern=r".*venmo-.+\.csv",

        # main account these transactions belong to
        target_account="Assets:Venmo:Account2",

        # if transactions have a unique id, it can be stored as a meta value &
        # checked in new imports
        duplication_key="meta.id",

        # benedict puts csv rows into a sub-dict "values"
        mapping_transactions="values",

        mapping_transaction={
            "date": "Datetime",

            "flag": lambda data: "*" if data.get("Status") == "Complete" else "!",

            "narration": lambda data: data.get("Note") or data.get("Destination"),

            "payee": lambda data: data.get("From") if data.get("Amount (total)")[0] == "+" else data.get("To"),

            # any meta information
            "meta": {
                "id": "ID",
                "type": "Type",
            },
            "postings": [
                {
                    "amount": lambda data: [data.get("Amount (total)").replace("$", ""), "USD"],
                    "meta": {
                        "venmodate": "Datetime",
                    },
                }
            ],
        },
    ),
]

# needed to disable beancount's own duplication detection (which interfers with
# the configured one above)
ingest(CONFIG, hooks=[])
