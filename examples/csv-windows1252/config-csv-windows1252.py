import yabci

# https://docs.python.org/3/library/csv.html
def read_csv_windows1252(filename):
    import csv
    ret = []
    with open(filename, newline='', encoding='windows-1252') as f:
        reader = csv.DictReader(f)
        for row in reader:
            ret.append(row)

    return {"values": ret}


CONFIG = [
    yabci.Importer(

        name="CSV Sample Importer Basic",

        prepare_data=read_csv_windows1252,

        target_account="Assets:FooBank:Account1",

        # where are transactions stored? benedict lib puts csv values into a
        # sub-dict "values". "values" contains a list, each entry is mapped
        # into a single transaction
        mapping_transactions="values",

        # how shall a sinle transaction be mapped?
        mapping_transaction={
            "date": "Date",
            "narration": "Description",
            "postings": [
                {
                    "amount": ["Amount", b"EUR"],
                }
            ],
        },
    ),
]
