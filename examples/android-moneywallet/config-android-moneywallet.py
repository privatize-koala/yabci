import yabci
from beancount.ingest.scripts_utils import ingest


CONFIG = [
    yabci.Importer(

        name="Moneywallet json Sample Importer",

        prepare_data=lambda filename: yabci.utils.read_json_from_zip(
            filename,
            r".*database\.json",
        ),

        target_account="Assets:Moneywallet:Account1",

        duplication_key="meta.moneywalletid",

        mapping_transactions="transactions",

        # how shall a sinle transaction be mapped?
        mapping_transaction={
            "date": "date",
            "narration": "description",
            "tags": {b"moneywallet"},
            "meta": {
                "created": "date",
                "moneywalletid": "id",
            },
            "postings": [
                {
                    "amount": [
                        # raw data contains money as cents. Use a lambda to calculate the
                        # correct format
                        lambda data: "%.2f" % (-1 * data.get("money") / 100.),
                        b"EUR",
                    ],
                }
            ],
        },
    ),
]

# needed to disable beancount's own duplication detection (which interfers with
# the configured one above)
ingest(CONFIG, hooks=[])
